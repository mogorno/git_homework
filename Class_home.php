<?php 
abstract class Transport {

	public $car_class = ["BMW","Bentley","Audi","Aston Martin","Acura","Buick","Bugatti"];
	public $bike_class = ["Doge","SUZUKI","YAMAHA","BMW","KTM"];
	public $driver_name = ["Anton","Sanya","Vovan","Poroshenko","Zelensky","Kostya"];
	public $max_spd = ['222','434','67','425','134','657'];

	abstract function rand_driver();
}

interface Speed{

	public function max_speed();
}

class Car extends Transport
{
	protected function count_transport()
	{
		$transport_array = array();
		$r = array();
		$cval = count($this->car_class);
		$bval = count($this->bike_class);
		$nval = count($this->driver_name);	
		$transport_array['car'] = $cval;
		$transport_array['bike'] = $bval;
		$transport_array['name'] = $nval;
		$r1 = rand(0, $transport_array['car']);
		$r2	= rand(0, $transport_array['bike']);
		$r3 = rand(0, $transport_array['name']);
		$r['car'] = $r1;
		$r['bike'] = $r2;
		$r['name'] = $r3;
		return $r;
	}
	public function rand_driver()
	{	
		$a = $this->count_transport();
		$dn = $this->driver_name;
		$cc = $this->car_class;
		echo $dn[$a['name']] . " have car name: ". $cc[$a['car']] . "<hr>";
	}
}

class Bike extends Car implements Speed  {
	
	
	public function rand_driver()
	{
		$a = $this->count_transport();
		$dn = $this->driver_name;
		$bc = $this->bike_class;
		echo $dn[$a['name']] . " have car name: ". $bc[$a['car']];
	}
	public function max_speed()
	{
		$spd = rand(0 , count($this->max_spd));
		echo " max speed: " . $this->max_spd[$spd] ;
	}
}

$car = new Car();
$bike = new Bike();
echo '<div>' . $car->rand_driver() . '</div>';
echo '<div>' . $bike->rand_driver() . $bike->max_speed() . '</div>';
